/// Returns the prime numbers in `elements`.
// Les bornes de la deuxième boucle ne sont pas correctes.


func primes(_ numbers: [Int]) -> [Int] {
  var primes: [Int] = []

  for n in numbers {
    var isPrime = true
    for m in stride(from: 2, through:n-1, by: 1){
      if n % m == 0 {
        isPrime = false
        break
      }
    }

    if isPrime {
      primes.append(n)
    }
  }

  return primes
}

let array = Array(2 ..< 50)
let primesInArray = primes(array)
print("\(array) contains the following prime numbers: \(primesInArray)")
