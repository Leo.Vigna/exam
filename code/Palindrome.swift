/// Returns whether or not the given text is a palindrome.
func isPalindrome(_ text: String) -> Bool {

  // Write your code here.
  //
  // - Hint:
  //   Check the official documentation on Swift.String to see how to manipulate string
  //   indices (https://developer.apple.com/documentation/swift/string).


  // L'idée est de séparer le String en deux parties égales, d'inverser la seconde partie
  // et de comparer les deux sous-chaines. Si équivalente alors il s'agit d'un palindrome
  // sinon non.

  var is_palindrome = false
  let split = text.count/2
  var index = text.index(text.startIndex,offsetBy:split)
  let firstpart = text[..<index]

  index = text.index(text.endIndex,offsetBy: -split)
  let lastpart = text[index...]

  if firstpart == String(lastpart.reversed()){
    is_palindrome = true;
  }
  else{
    is_palindrome = false;
  }
  return is_palindrome
}

for string in ["kayak", "koala"] {
  let isStringAPalindrome = isPalindrome(string)
  print("'\(string)' is \(isStringAPalindrome ? "" : "not ")a palindrome")
}